#include <iostream>
#include <fstream>
#include "Visualisation.h"
#include "../FeatureExtractionLib/SharedPtrLinFunctionRealization.h"

using namespace std;
using namespace fe;
using namespace cv;

int main()
{
	setlocale(LC_ALL, "ru");
	// ������� ������, ������������� �� ������ � ����������
	auto nn = CreatePolynomialManager();
	cout << "��������� CreatePolynomialManager();\n";

	// ���������� ����� ����������� ���������
	int n_max;
	int diameter = 100;
	cout << "������� ������������ ���������� ������� ���������: ";
	cin >> n_max;
	nn->InitBasis(n_max, diameter);
	cout << "��������� InitBasis();\n";

	// ���������� �����
	OrthoBasis pln = nn->GetBasis();
	ShowPolynomials("����������� ������", pln);

	/*for (int i = 0; i < n_max; i++)
	{
		for (int j = 0; j < n_max; j++)
		{
			pln[i][j].first += 250;
			pln[i][j].second += 250;
		}
	}*/

	//ofstream out("ortho_li.txt");
	//Mat razl_real = Mat::zeros(n_max, n_max, CV_64FC1);
	//Mat razl_im = Mat::zeros(n_max, n_max, CV_64FC1);

	//for (int i = 0; i < n_max; i++)
	//{
	//	for (int j = 0; j < n_max; j++)
	//	{
	//		Mat real = pln[i][j].first;
	//		Mat image = -pln[i][j].second;
	//		
	//		Mat MulReal = real.mul(pln[0][0].first);
	//		Mat MulImage = image.mul(pln[0][0].second);

	//		double sumReal = sum(MulReal)[0];
	//		double sumImage = sum(MulImage)[0];

	//		razl_real.at<double>(i, j) = sumReal;
	//		razl_im.at<double>(i, j) = sumImage;
	//	}
	//}
	//out << razl_real << endl;
	//out.close();
	// ������� ���������� � ������
	string infoBas = nn->GetType();
	cout << "��������� PolynomialManager::GetType();\n" << infoBas << "\n";

	// ��������� ���� � ������������
	Mat image = imread("../test1.png", IMREAD_GRAYSCALE);
	cout << "��������� imread();\n";

	// ������� ���������� ������� ��������
	auto na = CreateBlobProcessor();
	cout << "��������� CreateBlobProcessor();\n\n";

	// ������� ������� ������� �� �������������� �����������
	vector<Mat> unnormalize = na->DetectBlobs(image);
	cout << "��������� DetectBlobs();\n";

	// �������� ������� ������� � ������� ��������
	vector<Mat> normalize = na->NormalizeBlobs(unnormalize, diameter);
	cout << "��������� NormalizeBlobs();\n";

	// ������� ���������� � ���� ����������� ������� ��������
	string infoNorm = na->GetType();
	cout << "��������� IBlobProcessor::GetType();\n";

	cout << "���������� ������� ��������: " << normalize.size() << endl;

	// ������������ � ���
	vector<Mat> rec;
	rec.resize(normalize.size());
	for (int i = 0; i < normalize.size(); i++)
	{
		ComplexMoments dec = nn->Decompose(normalize[i]);
		rec[i] = nn->Recovery(dec);
		ShowBlobDecomposition(to_string(i), normalize[i], rec[i]);
		waitKey();
	}

	//������� ������� �� ������������
	/*for (int i = 0; i < unnormalize.size(); i++)
	{
		imshow(to_string(i), unnormalize[i]);
	}*/

	//������� ������� ����� ������������
	/*for (int i = 0; i < normalize.size(); i++)
	{
		imshow(to_string(i), normalize[i]);
	}*/

	//��������������� �����������
	/*for (int i = 0; i < rec.size(); i++)
	{
		imshow(to_string(i), rec[i]);
	}*/

	//waitKey();
	return 0;
}