#pragma once
#include "MomentsRecognizer.h"

using namespace std;
using namespace cv;
using namespace fe;

class MomentsRecognizerRealization : public MomentsRecognizer
{
public:
	Mat MomentsToInput(ComplexMoments& moments);

	string OutputToValue(Mat output);

	bool Train(std::map<std::string, std::vector<fe::ComplexMoments>> moments, std::vector<int> layers,
		int max_iters,
		float eps,
		float speed);
};