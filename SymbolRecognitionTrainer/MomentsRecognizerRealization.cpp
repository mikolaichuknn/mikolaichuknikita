#include "MomentsRecognizerRealization.h"

using namespace std;
using namespace cv;
using namespace fe;

Mat MomentsRecognizerRealization::MomentsToInput(ComplexMoments& moments)
{
	// Find row/col size
	int rowSize = moments.abs.rows;
	int colSize = moments.abs.cols;

	Mat momentsConvert = Mat::zeros(rowSize, colSize, CV_32F);
	moments.abs.convertTo(momentsConvert, CV_32F);

	Mat momentsToInp = Mat::zeros(1, rowSize * colSize, CV_32F);

	// Assign
	for (int i = 0; i < rowSize; i++)
	{
		for (int j = 0; j < colSize; j++)
		{
			momentsToInp.at<float>(0, i * colSize + j) = momentsConvert.at<float>(i, j);
		}
	}
	return momentsToInp;
}

string MomentsRecognizerRealization::OutputToValue(Mat output)
{
	// Find row/col size
	int rowSize = output.rows;
	int colSize = output.cols;

	string value = "";
	float max = output.at<float>(0, 0);
	int index = 0;
	for (int j = 0; j < colSize; j++)
	{
		if (max < output.at<float>(0, j))
		{
			max = output.at<float>(0, j);
			index = j;
		}
	}
	value = values[index].c_str();
	return value;
}

bool MomentsRecognizerRealization::Train(std::map<std::string, std::vector<fe::ComplexMoments>> moments, std::vector<int> layers,
	int max_iters,
	float eps,
	float speed)
{
	// ������������ ��������� ��������
	Mat confLayers = Mat::zeros(1, layers.size() + 2, CV_32S);
	auto iter = moments.begin();
	int momentsSize = iter->second[0].abs.cols;
	confLayers.col(0) = momentsSize * momentsSize;
	for (int i = 0; i < layers.size(); i++)
	{
		confLayers.col(i + 1) = layers[i];
	}
	confLayers.col(layers.size() + 1) = 9;
	cout << confLayers << endl;

	pAnn = ml::ANN_MLP::create();
	pAnn->setLayerSizes(confLayers);
	pAnn->setActivationFunction(ml::ANN_MLP::SIGMOID_SYM, 1., 1.);
	pAnn->setBackpropMomentumScale(speed);
	pAnn->setBackpropWeightScale(speed);

	// �������� ��������� ��������
	TermCriteria term_criteria(CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, max_iters, eps);
	pAnn->setTermCriteria(term_criteria);
	pAnn->setTrainMethod(ml::ANN_MLP::RPROP, 0.001);

	// ������� � �������� ������� ��� train()
	Mat in;
	Mat out;

	// ���������� ��� ������� ����������
	for (auto it = moments.begin(); it != moments.end(); it++)
	{
		// input/output �������-������ (��� ������� sample)
		Mat inputArray = Mat::zeros(it->second.size(), it->second[0].abs.rows * it->second[0].abs.cols, CV_32F);
		Mat outputArray = Mat::zeros(it->second.size(), 9, CV_32F);

		Mat outArrayRow = Mat::zeros(1, 9, CV_32F);
		for (int i = 0; i < outArrayRow.cols; i++)
		{
			if (i == atoi(it->first.c_str()))
			{
				outArrayRow.at<float>(0, i) = 1.;
			}
			else
			{
				outArrayRow.at<float>(0, i) = 0.;
			}
		}

		for (int j = 0; j < it->second.size(); j++)
		{
			Mat inputRow = MomentsRecognizerRealization::MomentsToInput(it->second[j]);
			inputArray.push_back(inputRow);
			outputArray.push_back(outArrayRow);
		}

		values.push_back(it->first);

		cout << "Number to train: " << values[atoi(it->first.c_str())] << endl << endl;
		cout << "Input-matrix size for number " << it->first << ":" << inputArray.size() << endl;
		cout << "Output-matrix size for number " << it->first << ":" << outputArray.size() << endl;

		in.push_back(inputArray);
		out.push_back(outputArray);
	}
	//cout << in.size() << endl;
	//cout << out.size() << endl << endl;
	//cout << out << endl << endl;
	cout << "\n*** TRAIN DATA ***\n";
	cout << "Input-matrix size to train: " << in.size() << endl;
	cout << "Output-matrix size to train: " << out.size() << endl;

	cout << "\n*** TRAIN STARTING ***\n\n";
	pAnn->train(in, ml::ROW_SAMPLE, out);

	if (pAnn->isTrained())
	{
		cout << "Training was completed successfully!" << endl;
		return true;
	}
	else
	{
		return false;
	}
}