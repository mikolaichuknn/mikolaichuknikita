#include <iostream>

#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"

#include "../FeatureExtractionLib/SharedPtrLinFunctionRealization.h"
#include "MomentsHelper.h"
#include "MomentsRecognizerRealization.h"

using namespace cv;
using namespace std;
using namespace fe;

void generateData()
{
	// �1 ������������� ����������� ������ �� ������
	string labeled_data_path = "../Data/labeled_data";
	string ground_data_path = "../Data/ground_data";
	string test_data_path = "../Data/test_data";
	double percent = 80.;

	bool isDistribute = MomentsHelper::DistributeData(labeled_data_path, ground_data_path, test_data_path, percent);
	if (isDistribute)
	{
		cout << "++++Distribute data was completed successfully++++" << endl;
	}
	else
	{
		cout << "----Distribute data was failed----" << endl;
	}

	// �2 ��������� �������� �������� � ��������� ������ �������� ���� �� �����
	auto blobProc = CreateBlobProcessor();
	auto polyManage = CreatePolynomialManager();
	map<string, vector<ComplexMoments>> moments_ground;
	map<string, vector<ComplexMoments>> moments_test;

	// ground data
	polyManage->InitBasis(20, 100);
	bool isGenerateGround = MomentsHelper::GenerateMoments(ground_data_path, blobProc, polyManage, moments_ground);
	if (isGenerateGround)
	{
		cout << "++++Generate ground-data was completed successfully++++" << endl;
	}
	else
	{
		cout << "----Generate ground-data was failed----" << endl;
	}

	// test data
	bool isGenerateTest = MomentsHelper::GenerateMoments(test_data_path, blobProc, polyManage, moments_test);
	if (isGenerateTest)
	{
		cout << "++++Generate test-data was completed successfully++++" << endl;
	}
	else
	{
		cout << "----Generate test-data was failed----" << endl;
	}

	// �3 ���������� ���� �������� � �����
	// ground data
	bool isSaveGroundMoments = MomentsHelper::SaveMoments("GroundMoments.txt", moments_ground);
	if (isSaveGroundMoments)
	{
		cout << "++++Save ground-data was completed successfully++++" << endl;
	}
	else
	{
		cout << "----Save ground-data was failed----" << endl;
	}

	// test data
	bool isSaveTestMoments = MomentsHelper::SaveMoments("TestMoments.txt", moments_test);
	if (isSaveTestMoments)
	{
		cout << "++++Save test-data was completed successfully++++" << endl;
	}
	else
	{
		cout << "----Save test-data was failed----" << endl;
	}
}

void trainNetwork()
{
	// ������� ������ ��� �������������
	// �1
	MomentsRecognizerRealization recognizeObj;

	// �2
	map<string, vector<ComplexMoments>> moments;
	bool isRead = MomentsHelper::ReadMoments("GroundMoments.txt", moments);
	if (isRead)
	{
		cout << "++++Read moments was completed successfully++++" << endl;
	}
	else
	{
		cout << "----Read moments was failed----" << endl;
	}
	// ������������ ������� �����
	vector<int> layers;
	layers.push_back(50);
	layers.push_back(50);

	// �3
	// ��������
	bool isTrained = recognizeObj.Train(moments, layers, 10000, 0.001, 0.1);
	if (isTrained)
	{
		cout << "++++Train was completed successfully++++" << endl;
	}
	else
	{
		cout << "----Train moments was failed----" << endl;
	}

	// �4
	bool isSaveRec = recognizeObj.Save("TrainedNN.txt");
	if (isSaveRec)
	{
		cout << "++++Save trained NN was completed successfully++++" << endl;
	}
	else
	{
		cout << "----Save trained NN was failed----" << endl;
	}
}

void precisionTest()
{
	// �1 ������ ��� �������������
	MomentsRecognizerRealization recognizeObj;

	// �2 ���������� ��������� �� �� �����
	bool isReadNN = recognizeObj.Read("TrainedNN.txt");
	if (isReadNN)
	{
		cout << "++++Read trained NN was completed successfully++++" << endl;
	}
	else
	{
		cout << "----Read trained NN was failed----" << endl;
	}

	// �3 ���������� �������� ������
	map<string, vector<ComplexMoments>> moments;
	bool isReadMoments = MomentsHelper::ReadMoments("TestMoments.txt", moments);
	if (isReadMoments)
	{
		cout << "++++Read test-data from file was completed successfully++++" << endl;
	}
	else
	{
		cout << "----Read test-data from file was failed----" << endl;
	}

	// �4 ��������� ������ �������� ��������� �������������
	double precisionPercent = recognizeObj.PrecisionTest(moments) * 100.;
	cout << "Precision percent is " << precisionPercent << "%" << endl;
}

void recognizeImage()
{
	// �1 ��������� � ���������� ��� ��������
	setlocale(LC_CTYPE, "ru");
	string filename;
	cout << "������� ��� ��������: ";
	cin >> filename;
	
	auto nn = CreatePolynomialManager();
	cout << "��������� CreatePolynomialManager();\n";

	// ���������� ����� ����������� ���������
	int n_max = 20;
	int diameter = 100;
	nn->InitBasis(n_max, diameter);
	cout << "��������� InitBasis();\n";

	// ��������� ���� � ������������
	Mat image = imread(filename, IMREAD_GRAYSCALE);
	cout << "��������� imread();\n";

	// ������� ���������� ������� ��������
	auto na = CreateBlobProcessor();
	cout << "��������� CreateBlobProcessor();\n\n";

	// ������� ������� ������� �� �������������� �����������
	vector<Mat> unnormalize = na->DetectBlobs(image);
	cout << "��������� DetectBlobs();\n";

	// �������� ������� ������� � ������� ��������
	vector<Mat> normalize = na->NormalizeBlobs(unnormalize, diameter);
	cout << "��������� NormalizeBlobs();\n";

	cout << "���������� ������� ��������: " << normalize.size() << endl;

	MomentsRecognizerRealization recognizeObj;
	bool isReadNN = recognizeObj.Read("TrainedNN.txt");
	if (isReadNN)
	{
		cout << "++++Read trained NN was completed successfully++++" << endl;
	}
	else
	{
		cout << "----Read trained NN was failed----" << endl;
	}

	// ������������ � ���
	for (int i = 0; i < normalize.size(); i++)
	{
		ComplexMoments dec = nn->Decompose(normalize[i]);
		string recog = recognizeObj.Recognize(dec);
		cout << "������������ �����: " << recog << endl;
		imshow("����� " + recog, normalize[i]);
		waitKey(0);
	}
}

int main(int argc, char** argv)
{
	string key;
	do 
	{
		cout << "===Enter next values to do something:===" << endl << endl;
		cout << "  '1' - to generate data." << endl;
		cout << "  '2' - to train network." << endl;
		cout << "  '3' - to check recognizing precision." << endl;
		cout << "  '4' - to recognize single image." << endl;
		cout << "  'exit' - to close the application." << endl << endl;
		cin >> key;
		cout << endl;
		if (key == "1") {
			generateData();
		}
		else if (key == "2") {
			trainNetwork();
		}
		else if (key == "3") {
			precisionTest();
		}
		else if (key == "4") {
			recognizeImage();
		}
		cout << endl;
	} while (key != "exit");
	return 0;
}