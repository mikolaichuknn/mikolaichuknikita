#pragma once
#define ANNDLL_EXPORTS
#include <iostream>
#include <fstream>
#include <ANNRealize.h>

using namespace std;
using namespace ANN;

int main()
{
	//*.*.*.*.*.*.*.*
	//������� 3 (����). ���������� ���������� ANNTrainer
	//*.*.*.*.*.*.*.*

	setlocale(LC_ALL, "ru");
	vector<vector<float>> input_data;
	vector<vector<float>> output_data;
	
	//������ ��������� ������ �� ����� LearningData.txt
	LoadData("LearningData.txt", input_data, output_data);

	//������ ������������ ������������ ����� ������� ��������� ����
	vector<size_t> config;
	config.push_back(2);
	config.push_back(10);
	config.push_back(10);
	config.push_back(1);
	float scale = 1.0;

	ANeuralNetwork::ActivationType type = ANeuralNetwork::ActivationType::POSITIVE_SYGMOID;

	//������� ���� ������������, ��������� � vector<...> config
	auto nn = CreateNeuralNetwork(config, type, scale);

	//������� ��������� ���� ������� ��������� ��������������� ������
	
	BackPropTraining(nn, input_data, output_data, 100000, 0.001, 0.1, true);

	/*vector<size_t> configFromGetConfiguration = obj.GetConfiguration();
	for (int i = 0; i < configFromGetConfiguration.size(); i++) {
		cout << "����� ����: " << i << " ����� �������� � ������ ����: " << configFromGetConfiguration[i] << endl;
	}
	string typeFromGetType = obj.GetType();*/

	/*string str = obj.GetType();
	cout << str;*/

	//������� ���������� � ���� ��������� ����
	string str = nn->GetType();
	cout << str;

	//������� ������������ ��������� ����
	config.clear();
	config = nn->GetConfiguration();

	cout << "\nNetwork configuration: ";
	for (int i = 0; i < config.size(); i++) {
		cout << config[i];
		if (i != config.size() - 1) cout << "-";
	}

	cout << "\n";

	//��������� ��������� ��������� ���� � ���� 123.txt
	bool f = nn->Save("123.txt");

	config.clear();

	system("pause");
	return 0;
}