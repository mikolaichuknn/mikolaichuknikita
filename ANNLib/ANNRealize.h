#pragma once
#define ANNDLL_EXPORTS
#include <ANN.h>

using namespace std;

namespace ANN
{
	class ANNRealize : public ANeuralNetwork
	{
	public:
		ANNRealize();
		ANNRealize(vector<size_t>& config, ActivationType activation_t, float scl);
		~ANNRealize();

		vector<vector<float>> ValueOfActivation;

		//��������� ������ �������� ����
		string GetType();

		//��������������� ������ �� ���������� �����
		vector<float> Predict(vector<float>& input);
	};
}
