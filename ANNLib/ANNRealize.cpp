#pragma once
#define ANNDLL_EXPORTS
#include <iostream>
#include <ANNRealize.h>

using namespace std;
using namespace ANN;

vector<vector<float>> ValueOfActivationFunctionForEveryNeuron;
vector<vector<float>> AverageErrorForEveryNeuron;

std::shared_ptr<ANeuralNetwork> ANN::CreateNeuralNetwork(vector<size_t>& configuration,
	ANN::ANNRealize::ActivationType activation_type, float scale)
{
	return std::make_shared<ANNRealize>(configuration, activation_type, scale);
}

ANNRealize::ANNRealize()
{
}

ANNRealize::~ANNRealize()
{
}

ANNRealize::ANNRealize(vector<size_t>& config, ActivationType activation_t, float scl)
{
	configuration.clear();
	for (int i = 0; i < config.size(); i++)
	{
		configuration.push_back(config[i]);
	}

	activation_type = activation_t;

	scale = scl;
}

//��������� ������ �������� ����
string ANNRealize::GetType()
{
	std::string result = "\nNeuralNetworkType: multilayer perceptron. ANN was created by Mikolaichuk Nikita.\n";
	return result;
}

//��������������� ������ �� ���������� �����
vector<float> ANNRealize::Predict(vector<float>& input)
{
	ValueOfActivation.clear();

	std::vector<float> Out;
	ValueOfActivationFunctionForEveryNeuron.clear();
	ValueOfActivation.resize(configuration.size());
	ValueOfActivationFunctionForEveryNeuron.resize(configuration.size());

	for (unsigned int layer_index = 0; layer_index < configuration.size(); layer_index++)
	{
		ValueOfActivationFunctionForEveryNeuron[layer_index].resize(configuration[layer_index]);
		ValueOfActivation[layer_index].resize(configuration[layer_index]);
	}

	for (unsigned int from_index = 0; from_index < input.size(); from_index++)
	{
		ValueOfActivation[0][from_index] = input[from_index];
	}

	for (unsigned int layer_index = 0; layer_index < configuration.size() - 1; layer_index++)
	{
		for (unsigned int from_index = 0; from_index < weights[layer_index].size(); from_index++)
		{
			for (unsigned int to_index = 0; to_index < weights[layer_index][from_index].size(); to_index++)
			{

				ValueOfActivation[layer_index + 1][to_index] += ValueOfActivation[layer_index][from_index]
					* weights[layer_index][from_index][to_index];
			}
		}

		for (int i = 0; i < ValueOfActivationFunctionForEveryNeuron[layer_index + 1].size(); i++)
		{
			ValueOfActivation[layer_index + 1][i] = Activation(ValueOfActivation[layer_index + 1][i]);
		}
	}

	ValueOfActivationFunctionForEveryNeuron = ValueOfActivation;

	for (int i = 0; i < configuration[configuration.size() - 1]; i++)
	{
		Out.push_back(ValueOfActivation[configuration.size() - 1][i]);
	}

	return Out;
	Out.clear();
}

float ANN::BackPropTrainingIteration(
	std::shared_ptr<ANN::ANeuralNetwork> ann,
	std::vector<float>& input,
	std::vector<float>& output,
	float speed)
{
	std::vector<size_t> config;
	config = ann->GetConfiguration();

	std::vector<float> Out;
	ValueOfActivationFunctionForEveryNeuron.clear();
	AverageErrorForEveryNeuron.clear();
	ValueOfActivationFunctionForEveryNeuron.resize(config.size());
	AverageErrorForEveryNeuron.resize(config.size());

	for (unsigned int layer_index = 0; layer_index < config.size(); layer_index++)
	{
		ValueOfActivationFunctionForEveryNeuron[layer_index].resize(config[layer_index]);
		AverageErrorForEveryNeuron[layer_index].resize(config[layer_index]);
	}

	for (unsigned int from_index = 0; from_index < input.size(); from_index++)
	{
		ValueOfActivationFunctionForEveryNeuron[0][from_index] = input[from_index];
	}

	for (unsigned int layer_index = 0; layer_index < config.size() - 1; layer_index++)
	{
		for (unsigned int from_index = 0; from_index < ann->weights[layer_index].size(); from_index++)
		{
			for (unsigned int to_index = 0; to_index < ann->weights[layer_index][from_index].size(); to_index++)
			{
				ValueOfActivationFunctionForEveryNeuron[layer_index + 1][to_index] += ValueOfActivationFunctionForEveryNeuron[layer_index][from_index]
					* ann->weights[layer_index][from_index][to_index];
			}
		}

		for (int i = 0; i < ValueOfActivationFunctionForEveryNeuron[layer_index + 1].size(); i++)
		{
			ValueOfActivationFunctionForEveryNeuron[layer_index + 1][i] = ann->Activation(ValueOfActivationFunctionForEveryNeuron[layer_index + 1][i]);
		}
	}

	for (int i = 0; i < config[config.size() - 1]; i++)
	{
		Out.push_back(ValueOfActivationFunctionForEveryNeuron[config.size() - 1][i]);
	}

	std::vector<float> predictResult;
	predictResult = Out;

	float error = 0;

	for (int i = 0; i < predictResult.size(); i++)
	{
		error += (output[i] - predictResult[i]) * (output[i] - predictResult[i]);
	}

	for (int layer_index = config.size() - 2; layer_index > 0; layer_index--)
	{
		for (unsigned int from_index = 0; from_index < ann->weights[layer_index].size(); from_index++)
		{
			for (unsigned int to_index = 0; to_index < ann->weights[layer_index][from_index].size(); to_index++)
			{
				if (layer_index == config.size() - 2)
				{
					AverageErrorForEveryNeuron[layer_index + 1][to_index] = output[to_index] - predictResult[to_index];
					AverageErrorForEveryNeuron[layer_index][from_index] += AverageErrorForEveryNeuron[layer_index + 1][to_index]
						* ann->weights[layer_index][from_index][to_index];
				}
				else
				{
					AverageErrorForEveryNeuron[layer_index][from_index] += AverageErrorForEveryNeuron[layer_index + 1][to_index]
						* ann->weights[layer_index][from_index][to_index];
				}
			}
		}
	}

	for (unsigned int layer_index = 0; layer_index < config.size() - 1; layer_index++)
	{
		for (unsigned int from_index = 0; from_index < ann->weights[layer_index].size(); from_index++)
		{
			for (unsigned int to_index = 0; to_index < ann->weights[layer_index][from_index].size(); to_index++)
			{
				if (layer_index == 0)
				{
					ann->weights[layer_index][from_index][to_index] += speed
						* AverageErrorForEveryNeuron[layer_index + 1][to_index]
						* input[from_index]
						* ann->ActivationDerivative(ValueOfActivationFunctionForEveryNeuron[layer_index + 1][to_index]);
				}
				else
				{
					ann->weights[layer_index][from_index][to_index] += speed
						* AverageErrorForEveryNeuron[layer_index + 1][to_index]
						* ValueOfActivationFunctionForEveryNeuron[layer_index][from_index]
						* ann->ActivationDerivative(ValueOfActivationFunctionForEveryNeuron[layer_index + 1][to_index]);
				}
			}
		}
	}

	return sqrt(error);
	Out.clear();
	config.clear();
	predictResult.clear();
}

float ANN::BackPropTraining(
	std::shared_ptr<ANN::ANeuralNetwork> ann,
	std::vector<std::vector<float>>& inputs,
	std::vector<std::vector<float>>& outputs,
	int maxIters, float eps, float speed, bool std_dump)
{
	//���1: ������������� �����
	int countOfIteration = 0;
	float error = 0;
	ann->RandomInit();


	while (countOfIteration <= maxIters)
	{
		for (int i = 0; i < inputs.size(); i++)
		{
			error += BackPropTrainingIteration(ann, inputs[i], outputs[i], speed);
		}

		error /= inputs.size();

		if (std_dump == true && countOfIteration % 100 == 0)
			std::cout << "Iteration: " << countOfIteration << "\t\tError: " << error << std::endl;

		countOfIteration++;
		if (error < eps)
		{
			break;
		}
		error = 0;
	}

	ann->is_trained = true;
	return 0;
}
