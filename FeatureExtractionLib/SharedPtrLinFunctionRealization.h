#pragma once
#define ANNDLL_EXPORTS
#include "LibFunctionRealization.h"

namespace fe
{
	std::shared_ptr<IBlobProcessor> CreateBlobProcessor()
	{
		return std::shared_ptr<IBlobProcessor>(new IBlobRealization());
	}

	std::shared_ptr<PolynomialManager> CreatePolynomialManager()
	{
		return std::shared_ptr<PolynomialManager>(new PolynomialManagerRealization());
	}
}