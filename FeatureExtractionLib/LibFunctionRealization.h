#pragma once
#define __declspec(dllimport)
#include <fstream>
#include "FeatureExtraction.h"
#include "RadialFunctions.h"

using namespace std;
using namespace fe;
using namespace cv;

class IBlobRealization : public fe::IBlobProcessor
{
public:

	// ** IBlobProcessor **
	string GetType()
	{
		std::string result = "\nIBlobProcessor was created by Mikolaichuk Nikita.\n";
		return result;
	}

	vector<Mat> DetectBlobs(Mat image)
	{
		vector<vector<Point>> contours;
		vector<Vec4i> hierarchy;
		threshold(image, image, 127, 255, CV_THRESH_BINARY_INV);
		findContours(image, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE);

		vector<Mat> output;
		for (int idx = 0; idx != -1; idx = hierarchy[idx][0])
		{
			Point2f center;
			float rad = 0;
			minEnclosingCircle(contours[idx], center, rad);
			
			Mat contourImage = Mat(Size(2.2 * rad, 2.2 * rad), CV_8UC1, Scalar(0));
			drawContours(contourImage, contours, idx, Scalar(255), FILLED, LINE_8, hierarchy, rad, Point(-center.x + rad * 1.05, -center.y + rad * 1.0));
			output.push_back(contourImage);
		}
		return output;
	}

	vector<Mat> NormalizeBlobs(vector<Mat>& blobs, int side)
	{
		vector<Mat> norm;
		norm.resize(blobs.size());
		for (int i = 0; i < blobs.size(); i++)
		{
			norm[i] = Mat::zeros(Size(side, side), CV_8UC1);
			resize(blobs[i], norm[i], Size(side, side));
		}
		return norm;
	}
	// ** \\ ** // ** \\ ** //
};

class PolynomialManagerRealization : public fe::PolynomialManager
{
public:
	// ** PolynomialManager **
	string GetType()
	{
		string result = "\nPolynomialManager was created by Mikolaichuk Nikita.\n";
		return result;
	}

	void InitBasis(int n_max, int diameter)
	{
		double radius = (double)diameter / 2.0;
		polynomials.clear();
		polynomials.resize(n_max);
		for (int n = 0; n < n_max; n++)
		{
			polynomials[n].resize(n_max);
			for (int m = 0; m < n_max; m++)
			{
				polynomials[n][m].first = Mat::zeros(diameter, diameter, CV_64FC1);
				polynomials[n][m].second = Mat::zeros(diameter, diameter, CV_64FC1);
				for (int i = 0; i < diameter; i++)
				{
					for (int j = 0; j < diameter; j++)
					{
						double x = (radius - (double)i);
						double y = (radius - (double)j);
						double fi = atan2(y, x);

						double chebFunc = rf::RadialFunctions::ShiftedChebyshev(sqrt(x * x + y * y) / radius, n);

						polynomials[n][m].first.at<double>(i, j) = chebFunc * cos(m * fi);
						polynomials[n][m].second.at<double>(i, j) = chebFunc * sin(m * fi);
					}
				}

				//Mat real = Mat::zeros(diameter, diameter, CV_64FC1);
				//Mat image = Mat::zeros(diameter, diameter, CV_64FC1);
				//Mat MulReal = Mat::zeros(diameter, diameter, CV_64FC1);
				//Mat MulImage = Mat::zeros(diameter, diameter, CV_64FC1);

				//real = polynomials[n][m].first;
				//image = -polynomials[n][m].second;

				//MulReal = real.mul(polynomials[n][m].first);
				//MulImage = image.mul(polynomials[n][m].second);

				//double sumReal = sum(MulReal)[0];
				//double sumImage = sum(MulImage)[0];

				//double summ = sqrt(sumReal * sumReal + sumImage * sumImage);

				//polynomials[n][m].first /= sqrt(summ);
				//polynomials[n][m].second /= sqrt(summ);
			}
		}
	}

	ComplexMoments Decompose(Mat blob)
	{
		// n_max - ������������ ������� ��������(����� ��������� - n_max * n_max), blob_size - ������ ����������� ������� �������
		int n_max = polynomials.size();
		int blob_size = blob.rows;

		// �������� ��� ������� ������� �� CV_8UC1(uint) � CV_64FC1(double)
		Mat convBlob = Mat::zeros(blob_size, blob_size, CV_64FC1);
		blob.convertTo(convBlob, CV_64FC1);

		ComplexMoments decomp;
		// �������������� ���� ComplexMoments, ������ - n_max * n_max(�.�. ����� �������� = ����� ���������)
		decomp.re = Mat::zeros(n_max, n_max, CV_64FC1);
		decomp.im = Mat::zeros(n_max, n_max, CV_64FC1);
		decomp.abs = Mat::zeros(n_max, n_max, CV_64FC1);
		decomp.phase = Mat::zeros(n_max, n_max, CV_64FC1);
		double norm = 0.0;
		for (int i = 0; i < n_max; i++)
		{
			for (int j = 0; j < n_max; j++)
			{
				// ����������� ������������� ��������� ������� ������� �� ������� �������� ������� ��� �� �����������
				// ������� ����������� ������� ������� � ��������� ���������� - (blob_size x blob_size)
				// MatAfterMul_real/image - ��������� ���������

				Mat MatPoly_real = polynomials[i][j].first;
				Mat MatPoly_image = -polynomials[i][j].second;

				Mat MatAfterMul_real = MatPoly_real.mul(convBlob);
				Mat MatAfterMul_image = MatPoly_image.mul(convBlob);

				// ������� ����� ���� ��������� 
				double sumMat_real = sum(MatAfterMul_real)[0];
				double sumMat_image = sum(MatAfterMul_image)[0];

				// ����� ������� ���� �������� �������� (��� ��������� �����)
				norm += sqrt(sumMat_real * sumMat_real + sumMat_image * sumMat_image);

				// ���������� ������������ ������� � ���� ComplexMoments
				decomp.re.at<double>(i, j) = sumMat_real;
				decomp.im.at<double>(i, j) = sumMat_image;
			}
		}
		for (int i = 0; i < n_max; i++)
		{
			for (int j = 0; j < n_max; j++)
			{
				// ��������� re � im
				decomp.re.at<double>(i, j) /= norm;
				decomp.im.at<double>(i, j) /= norm;
				decomp.abs.at<double>(i, j) = sqrt(decomp.re.at<double>(i, j) * decomp.re.at<double>(i, j) + decomp.im.at<double>(i, j) * decomp.im.at<double>(i, j));
				decomp.phase.at<double>(i, j) = atan2(decomp.im.at<double>(i, j), decomp.re.at<double>(i, j));
			}
		}
		return decomp;
	}

	Mat Recovery(ComplexMoments& decomposition)
	{
		// n_max - ������������ ������� ���������(����� ��������� - n_max * n_max), matMax - ������� ����������� ���������
		int n_max = polynomials.size();
		int matMax = polynomials[0][0].first.rows;
		// �������������� �������� �����������
		Mat output = Mat::zeros(matMax, matMax, CV_64FC1);

		// ���������� �������� � ������ ����� ����� ��������� ��������������
		Mat real = Mat::zeros(matMax, matMax, CV_64FC1);
		Mat image = Mat::zeros(matMax, matMax, CV_64FC1);

		// ��� ��������������: ������������ �������(�����) �� �������(��������)
		for (int i = 2; i < n_max; i++)
		{
			for (int j = 0; j < n_max; j++)
			{
				// moment = (a + jb); polynomial = (c + jd)
				// recovery_image = sum(moment[i] * polynomial[i])
				// moment * polynomial = ac-bd + j(ad+bc)
				// real += ac - bd;
				real += (decomposition.re.at<double>(i, j) * polynomials[i][j].first)
					- (decomposition.im.at<double>(i, j) * polynomials[i][j].second);
				// image += ad + bc;
				image += (decomposition.re.at<double>(i, j) * polynomials[i][j].second)
					+ (decomposition.im.at<double>(i, j) * polynomials[i][j].first);
			}
		}
		// �������� ����������� - ������
		for (int n = 0; n < matMax; n++)
		{
			for (int m = 0; m < matMax; m++)
			{
				output.at<double>(n, m) = sqrt(real.at<double>(n, m) * real.at<double>(n, m) + image.at<double>(n, m) * image.at<double>(n, m));
			}
		}
		//output = real;

		ofstream out("output_image.txt");
		out << image;
		out.close();

		ofstream outt("output_real.txt");
		outt << decomposition.re;
		outt.close();

		return output;
	}

	OrthoBasis GetBasis()
	{
		return polynomials;
	}
	// ** \\ ** // ** \\ ** //
};