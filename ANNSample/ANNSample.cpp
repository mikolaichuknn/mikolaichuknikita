#pragma once
#define ANNDLL_EXPORTS
#include <iostream>
#include <ANNRealize.h>

using namespace std;
using namespace ANN;

int main()
{
	//*.*.*.*.*.*.*.*
	//������� 4 (����). ���������� ���������� ANNSample
	//*.*.*.*.*.*.*.*

	setlocale(LC_ALL, "ru");

	vector<size_t> config;
	float scale = 1.0;

	ANeuralNetwork::ActivationType type = ANeuralNetwork::ActivationType::POSITIVE_SYGMOID;

	//������� ��������� ����
	auto nn = ANN::CreateNeuralNetwork(config, type, scale);

	//������ ��������� ���� �� ����� 123.txt
	nn->Load("../ANNTrainer/123.txt");
	//nn->Load("../Debug/xor.ann");

	//������� ���������� � ���� ��������� ����
	cout << nn->GetType();

	vector<vector<float>> input;
	vector<vector<float>> output;

	vector<vector<float>> predictOut;

	//������ �������� ������ �� ����� LearningData.txt
	LoadData("../ANNTrainer/LearningData.txt", input, output);

	//������������� ����� ���� �� ��� ����� (������ ���� �� ���� �������� ������)
	for (int i = 0; i < input.size(); i++) {
		predictOut.push_back(nn->Predict(input[i]));
	}

	//cout << predictOut.size();

	//������� ����������
	cout << "\n\n������� ������\t\t�������� ������\t\t������������� �����\n\n";
	for (int i = 0; i < input.size(); i++) {
		for (int j = 0; j < input[i].size(); j++) {
			cout << input[i][j] << "\t";
		}
		cout << "\t\t";
		for (int j = 0; j < output[i].size(); j++) {
			cout << output[i][j] << "\t";
		}
		cout << "\t\t";
		for (int j = 0; j < predictOut[i].size(); j++) {
			cout << predictOut[i][j] << "\t\t";
		}
		cout << "\n";
	}

	system("pause");
	return 0;
}