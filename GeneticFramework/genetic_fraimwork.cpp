#pragma once
#include <iostream>
#include <algorithm>
#include <locale.h>
#include "Individual.h"
#include "GeneticAlgorithm.h"

using  namespace std;
using namespace ga;

int main()
{
	setlocale(LC_ALL, "ru");
	// Hyperparameters
	int epoch_size = 300;
	int ann_size = 50;
	double unchange_perc = 10;
	double mutation_perc = 90;
	double crossover_perc = 0;

	vector<size_t> config;
	config.push_back(2);
	config.push_back(10);
	config.push_back(10);
	config.push_back(1);

	ANeuralNetwork::ActivationType type = ANeuralNetwork::ActivationType::POSITIVE_SYGMOID;

	std::shared_ptr<ga::Epoch> plEpoch = make_shared<Epoch>();
	plEpoch->population.resize(ann_size);

	// ������� ��������� ���������
	for (int i = 0; i < ann_size; i++)
	{
		auto nn = CreateNeuralNetwork(config, type);
		nn->RandomInit();

		// �������������� ������-�������
		std::shared_ptr<ga::Individual> Ind = make_shared<Individual>();
		Ind->pAnn = nn;
		Ind->error = 0.;

		plEpoch->population[i].first = 0;
		plEpoch->population[i].second = Ind->Clone();
	}

	vector<vector<float>> input;
	vector<vector<float>> output;

	//������ �������� ������ �� ����� LearningData.txt
	LoadData("LearningData.txt", input, output);

	std::shared_ptr<ga::Individual> nn = make_shared<Individual>();
	// �������������� ������, ������������� ������������ ��������
	std::shared_ptr<ga::GeneticAlgorithm> genAlg = make_shared<GeneticAlgorithm>();

	for (int ep = 0; ep < epoch_size; ep++)
	{
		plEpoch->EpochBattle();
		genAlg->epoch = plEpoch;

		nn = dynamic_pointer_cast<Individual>(genAlg->epoch->population[0].second);

		cout << "Epoch: " << ep << "\tAvg. error: " << nn->error << endl;
		plEpoch = genAlg->Selection(unchange_perc, mutation_perc, crossover_perc);
	}

	vector<vector<float>> predictOut;

	plEpoch->EpochBattle();
	sort(plEpoch->population.begin(), plEpoch->population.end(),
		[](std::pair<int, pIIndividual> a, std::pair<int, pIIndividual> b)
		{
			return a.first > b.first;
		});

	nn = dynamic_pointer_cast<Individual>(plEpoch->population[0].second);

	//������������� ����� ���� �� ��� ����� (������ ���� �� ���� �������� ������)
	for (int i = 0; i < input.size(); i++)
	{
		predictOut.push_back(nn->pAnn->Predict(input[i]));
	}

	//������� ����������
	cout << "\n\n������� ������\t\t�������� ������\t\t������������� �����\n\n";
	for (int i = 0; i < input.size(); i++)
	{
		for (int j = 0; j < input[i].size(); j++)
		{
			cout << input[i][j] << "\t";
		}
		cout << "\t\t";
		for (int j = 0; j < output[i].size(); j++)
		{
			cout << output[i][j] << "\t";
		}
		cout << "\t\t";
		for (int j = 0; j < predictOut[i].size(); j++)
		{
			cout << predictOut[i][j] << "\t\t";
		}
		cout << "\n";
	}

	return 0;
}