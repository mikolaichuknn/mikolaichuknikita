#pragma once
#include <algorithm>
#include <random>
#include <cassert>
#include <iostream>
#include "GeneticAlgorithm.h"

using namespace ga;
using namespace std;

struct PRNG
{
	std::mt19937 engine;
};

void initGenerator1(PRNG& generator)
{
	// ������ ������-���������� ��� ��������� ���������� �����.
	std::random_device device;
	// �������� ��������� ����� ������������������
	generator.engine.seed(device());
}

// ���������� ����� ����� � ��������� [minValue, maxValue)
unsigned random1(PRNG& generator, unsigned minValue, unsigned maxValue)
{
	// ��������� ������������ ����������
	assert(minValue < maxValue);

	// ������ �������������
	std::uniform_int_distribution<unsigned> distribution(minValue, maxValue);

	// ��������� ��������������� �����: ������� ������������� ��� �������,
	//  ������� ��������� ������������ ����� ����� ��� ��������.
	return distribution(generator.engine);
}

// ���������� ����� � ��������� ������ � ��������� [minValue, maxValue)
float getRandomFloat1(PRNG& generator, float minValue, float maxValue)
{
	// ��������� ������������ ����������
	assert(minValue < maxValue);

	// ������ �������������
	std::uniform_real_distribution<float> distribution(minValue, maxValue);

	// ��������� ��������������� �����: ������� ������������� ��� �������,
	//  ������� ��������� ������������ ����� ����� ��� ��������.
	return distribution(generator.engine);
}

ga::GeneticAlgorithm::GeneticAlgorithm()
{}


ga::GeneticAlgorithm::~GeneticAlgorithm()
{}

ga::pEpoch ga::GeneticAlgorithm::Selection(double unchange_perc, double mutation_perc, double crossover_perc)
{
	PRNG generator;
	initGenerator1(generator);

	// ���������� ������ �� ��������� �����.
	sort(epoch->population.begin(), epoch->population.end(),
		[](std::pair<int, pIIndividual> a, std::pair<int, pIIndividual> b)
		{
			return a.first > b.first;
		});
	//throw "Stub called";

	// ����������� �����
	std::shared_ptr<ga::Epoch> pNextEpoch = make_shared<Epoch>();
	pNextEpoch->population.resize(epoch->population.size());

	int popSizeUnchange = (int)(epoch->population.size() * (unchange_perc / 100));
	int popSizeMutation = (int)(epoch->population.size() * (mutation_perc / 100));
	int popSizeCrossover = (int)(epoch->population.size() * (crossover_perc / 100));

	// ������� ��������(unchange_perc) ������ ������ 
	for (int i = 0; i < popSizeUnchange; i++)
	{
		pNextEpoch->population[i].first = 0;//epoch->population[i].first;
		pNextEpoch->population[i].second = epoch->population[i].second->Clone();
	}

	// ��������� ����� ��������� �������, ����������� ������������ ������ ������ ���������� �����
	for (int i = 0; i < popSizeCrossover; i++)
	{
		/*int index_1 = (double)(rand()) / RAND_MAX * (max - min) + min;
		int index_2 = (double)(rand()) / RAND_MAX * (max - min) + min;*/
		unsigned int index_1 = 0;
		unsigned int index_2 = 0;
		while (index_1 == index_2)
		{
			index_1 = random1(generator, 0, popSizeUnchange - 1);
			index_2 = random1(generator, 0, popSizeUnchange - 1);
		}

		//cout << "INDEX1: " << index_1 << "\tINDEX2:" << index_2 << endl;

		pNextEpoch->population[popSizeUnchange + i].first = 0;
		pNextEpoch->population[popSizeUnchange + i].second = 
			(pNextEpoch->population[index_1].second->Crossover(pNextEpoch->population[index_2].second))->Clone();
	}

	// ��������� ����� ��������� �������, ����������� �������� ������ ������ ���������� �����
	for (int i = 0; i < popSizeMutation; i++)
	{
		int index_1 = random1(generator, 0, popSizeUnchange - 1);
		//int index_1 = (double)(rand()) / RAND_MAX * (max - min) + min;

		pNextEpoch->population[popSizeUnchange + popSizeCrossover + i].first = 0;
		pNextEpoch->population[popSizeUnchange + popSizeCrossover + i].second = 
			(pNextEpoch->population[index_1].second->Mutation())->Clone();
	}
	return pNextEpoch;
}

