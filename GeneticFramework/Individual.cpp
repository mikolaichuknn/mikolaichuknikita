#pragma once
#include "Individual.h"
#include <iostream>
#include <random>
#include <cassert>

using namespace ga;
using namespace std;
using namespace ANN;

struct PRNG
{
	std::mt19937 engine;
};

void initGenerator(PRNG& generator)
{
	// ������ ������-���������� ��� ��������� ���������� �����.
	std::random_device device;
	// �������� ��������� ����� ������������������
	generator.engine.seed(device());
}

// ���������� ����� ����� � ��������� [minValue, maxValue)
unsigned random(PRNG& generator, unsigned minValue, unsigned maxValue)
{
	// ��������� ������������ ����������
	assert(minValue < maxValue);

	// ������ �������������
	std::uniform_int_distribution<unsigned> distribution(minValue, maxValue);

	// ��������� ��������������� �����: ������� ������������� ��� �������,
	//  ������� ��������� ������������ ����� ����� ��� ��������.
	return distribution(generator.engine);
}

// ���������� ����� � ��������� ������ � ��������� [minValue, maxValue)
float getRandomFloat(PRNG& generator, float minValue, float maxValue)
{
	// ��������� ������������ ����������
	assert(minValue < maxValue);

	// ������ �������������
	std::uniform_real_distribution<float> distribution(minValue, maxValue);

	// ��������� ��������������� �����: ������� ������������� ��� �������,
	//  ������� ��������� ������������ ����� ����� ��� ��������.
	return distribution(generator.engine);
}

ga::Individual::Individual()
{}

ga::Individual::~Individual()
{}

std::shared_ptr<ga::IIndividual> Individual::Mutation()
{
	PRNG generator;
	initGenerator(generator);

	double min = 0.0;
	double max = 0.0;

	for (unsigned int layer_index = 0; layer_index < pAnn->configuration.size() - 1; layer_index++)
	{
		for (unsigned int from_index = 0; from_index < pAnn->weights[layer_index].size(); from_index++)
		{
			for (unsigned int to_index = 0; to_index < pAnn->weights[layer_index][from_index].size(); to_index++)
			{
				min = pAnn->weights[layer_index][from_index][to_index] - delWeights * pAnn->weights[layer_index][from_index][to_index];
				max = pAnn->weights[layer_index][from_index][to_index] + delWeights * pAnn->weights[layer_index][from_index][to_index];

				if (min > max)
				{
					double buf = min;
					min = max;
					max = buf;
				}

				pAnn->weights[layer_index][from_index][to_index] = getRandomFloat(generator, min, max);
			}
		}
	}

	std::shared_ptr<ga::Individual> mutation = make_shared<Individual>();
	mutation->pAnn = pAnn;
	return mutation;
}

std::shared_ptr<ga::IIndividual> Individual::Crossover(std::shared_ptr<IIndividual> individual)
{
	PRNG generator;
	initGenerator(generator);

	std::shared_ptr<ga::Individual> individ_other = make_shared<Individual>();
	individ_other = dynamic_pointer_cast<Individual>(individual);

	std::shared_ptr<ga::Individual> individ_daughter = make_shared<Individual>();
	individ_daughter->pAnn = individ_other->pAnn;

	// ����������� ��������� ��������
	/*for (unsigned int layer_index = 0; layer_index < pAnn->configuration.size() - 1; layer_index++)
	{
		for (unsigned int from_index = 0; from_index < pAnn->weights[layer_index].size(); from_index++)
		{
			for (unsigned int to_index = 0; to_index < pAnn->weights[layer_index][from_index].size(); to_index++)
			{
				if (random(generator, 0, 1) == 0)
				{
					individ_daughter->pAnn->weights[layer_index][from_index][to_index] =
						pAnn->weights[layer_index][from_index][to_index];
				}
				else
				{
					individ_daughter->pAnn->weights[layer_index][from_index][to_index] =
						individ_other->pAnn->weights[layer_index][from_index][to_index];
				}
			}
		}
	}*/

	// ����������� �� �����
	for (unsigned int layer_index = 0; layer_index < pAnn->configuration.size() - 1; layer_index++)
	{
		if (random(generator, 0, 1) == 0)
		{
			individ_daughter->pAnn->weights[layer_index] = pAnn->weights[layer_index];
		}
		else
		{
			individ_daughter->pAnn->weights[layer_index] = individ_other->pAnn->weights[layer_index];
		}
	}
	return individ_daughter;
}

std::pair<int, int> Individual::Spare(std::shared_ptr<IIndividual> individual)
{
	vector<vector<float>> input;
	vector<vector<float>> output;

	//������ �������� ������ �� ����� LearningData.txt
	LoadData("LearningData.txt", input, output);

	vector<vector<float>> predict_own;
	vector<vector<float>> predict_other;

	std::shared_ptr<ga::Individual> individ_other = make_shared<Individual>();
	individ_other = dynamic_pointer_cast<Individual>(individual);

	//������������� ����� ���� �� ��� ����� (������ ���� �� ���� �������� ������)
	for (int i = 0; i < input.size(); i++)
	{
		predict_own.push_back(pAnn->Predict(input[i]));
		predict_other.push_back(individ_other->pAnn->Predict(input[i]));
	}

	error = 0.;
	int ownScore = 0;
	int otherScore = 0;

	pair<int, int> scoreResult = {0, 0};

	for (int i = 0; i < predict_own.size(); i++)
	{
		for (int j = 0; j < predict_own[i].size(); j++)
		{
			error += fabs(output[i][j] - predict_own[i][j]);
			individ_other->error += fabs((output[i][j] - predict_other[i][j]));
		}
	}

	error /= input.size();
	individ_other->error /= input.size();

	if (error < individ_other->error)
	{
		ownScore += score;
		otherScore -= score;
	}
	else if (error > individ_other->error)
	{
		ownScore -= score;
		otherScore += score;
	}

	scoreResult.first = ownScore;
	scoreResult.second = otherScore;
	return scoreResult;
}

std::vector<float> Individual::MakeDecision(std::vector<float>& input)
{
	return vector<float>();
}

std::shared_ptr<ga::IIndividual> Individual::Clone()
{
	std::shared_ptr<ga::Individual> clone = make_shared<Individual>();
	clone->pAnn = CreateNeuralNetwork(pAnn->configuration, pAnn->activation_type);
	clone->pAnn->activation_type = pAnn->activation_type;
	clone->pAnn->configuration = pAnn->configuration;
	clone->pAnn->is_trained = pAnn->is_trained;
	clone->pAnn->scale = pAnn->scale;
	clone->pAnn->weights = pAnn->weights;

	clone->delWeights = delWeights;
	clone->error = error;
	clone->score = score;
	return clone;
}