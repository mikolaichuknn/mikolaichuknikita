#pragma once
#include "IIndividual.h"
#include "../ANNLib/ANNRealize.h"

using namespace ANN;
using namespace std;

namespace ga
{
	class Individual : public IIndividual
	{
	protected:
		// ������� �� ���� ��� �������
		double delWeights = 0.2;

		// ������ / ������
		int score = 1;
	public:
		Individual();
		~Individual();

		// ��������� ����
		shared_ptr<ANeuralNetwork> pAnn;
		float error;
		//pIIndividual pIIndivid;

		/**
		 * ��������� ������� �����.
		 * @return ������������ �����.
		 */
		shared_ptr<IIndividual> Mutation();

		/**
		 * ��������� ����������� ������� ����� � ������ ������.
		 * @param individual - ����� � ������� ����� ��������� �����������.
		 * @return �������� ����� ����� �����������.
		 */
		shared_ptr<IIndividual> Crossover(shared_ptr<IIndividual> individual);

		/**
		 * �������� ������������ ����� ������� � ������ ������.
		 * @param individual - ������ �����.
		 * @return ���� ����. ������ �������� - ���������� ����� ��������� ������� ������.
		 *					  ������ �������� - ���������� �����, ��������� ������ ������.
		 */
		pair<int, int> Spare(shared_ptr<IIndividual> individual);

		/**
		 * ������� �������.
		 * � �������� ������������ ����� ���������� ��������� �������,
		 * �� ����� ������� ������� �������� ������������.
		 * @param input - ������� ������
		 * @return �������� ������.
		 */
		vector<float> MakeDecision(vector<float>& input);

		/**
		 * ����������� ������� �����.
		 * @return ����� ������� �����.
		 */
		shared_ptr<IIndividual> Clone();
	};
	// ��������������� ���� "����� ��������� �� �������".
	typedef std::shared_ptr<Individual> pIndividual;
};


